package tk.vibiana.myapp.delegates;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;

public class remoteText {
	private URL source;
	public remoteText(String urlString) throws MalformedURLException{
		source = new URL(urlString);
	}
	public remoteText(URL urlObject){
		source = urlObject;
	}
	public URL getSourceURL(){
		return source;
	}
	public String getContent() throws ClientProtocolException, IOException{
		StringBuffer intermediateResults = new StringBuffer();
	
		HttpClient client = new DefaultHttpClient();
		HttpGet getter = new HttpGet("http://www.textfiles.com/food/arcadian.txt");
		HttpResponse response = client.execute(getter);
		HttpEntity entity = response.getEntity();

		if (entity != null){
			InputStream entityContentStream = entity.getContent();
			try{
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entityContentStream));
				boolean readable = true;
				while(readable){
					String line = bufferedReader.readLine();
					if(line == null){
						readable = false;
					}
					else{
						intermediateResults.append(line);
					}
				}
			}
			catch (IOException ex){
				ex.printStackTrace();
			}
			catch (RuntimeException ex){
				ex.printStackTrace();
			
			}
		
	
		}
		return intermediateResults.toString();
	}

}
